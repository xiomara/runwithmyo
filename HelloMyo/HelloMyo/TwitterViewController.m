//
//  TwitterViewController.m
//  HelloMyo
//
//  Created by Xiomara on 9/20/15.
//  Copyright (c) 2015 Thalmic Labs. All rights reserved.
//

#import "TwitterViewController.h"

@interface TwitterViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation TwitterViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://twitter.com/_xiomara7"]]];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
