//
//  TLHMViewController.m
//  HelloMyo
//
//  Copyright (c) 2013 Thalmic Labs. All rights reserved.
//  Distributed under the Myo SDK license agreement. See LICENSE.txt.
//

#import <MyoKit/MyoKit.h>
#import <TwitterKit/TwitterKit.h>
#import <Fabric/Fabric.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

#import "TwitterViewController.h"
#import "TLHMViewController.h"

@interface TLHMViewController ()

@property (weak, nonatomic) IBOutlet UILabel *helloLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *accelerationProgressBar;
@property (weak, nonatomic) IBOutlet UILabel *accelerationLabel;
@property (weak, nonatomic) IBOutlet UILabel *armLabel;
@property (weak, nonatomic) IBOutlet UILabel *lockLabel;
@property (strong, nonatomic) TLMPose *currentPose;
@property float mi_magnitud;
@property int flag;
@property BOOL doCountdown;
@property BOOL isScore;
@property float prevMagnitude;
@property (weak, nonatomic) IBOutlet UILabel *gauge;
@property (strong, nonatomic) UILabel *username;

@property TWTRAPIClient *client;
@property TWTRLogInButton* logInButton;

@property NSURL *pointURL;
@property NSURL *dieURL;

@property AVAudioPlayer *dieAudioPlayer;
@property AVAudioPlayer *pointAudioPlayer;

@property int score;
@property NSString *userID;
@property int clock;
@property int timeFlag;
@property (weak, nonatomic) IBOutlet UILabel *clockView;
@property NSTimer *timer;
@property int remainingCounts;
@property UILabel *finish;
@property (weak, nonatomic) IBOutlet UIButton *tweetIt;

- (IBAction)didTapSettings:(id)sender;

@end

@implementation TLHMViewController

#pragma mark - View Lifecycle

- (id)init {
    // Initialize our view controller with a nib (see TLHMViewController.xib).
    self = [super initWithNibName:@"TLHMViewController" bundle:nil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.score=0;
    self.clock=20;
    self.timeFlag=0;
    self.doCountdown = YES;
    self.isScore = YES;
    self.remainingCounts = 10;
    self.username = [[UILabel alloc] initWithFrame:CGRectMake(194.0, 30.0, 84.0, 44.0)];
    
    [self.view addSubview:self.username];
    
    self.finish = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 200.0, 200.0)];
    self.finish.center = self.view.center;
    self.finish.alpha = 0.0;
    [self.view addSubview:self.finish];
    
    self.logInButton = [TWTRLogInButton buttonWithLogInCompletion:^(TWTRSession* session, NSError* error) {
        if (session) {
            NSLog(@"signed in as %@", [session userName]);
            self.userID = [session userID];
            self.username.text = [session userName];
            [self updateValues];
        } else {
            NSLog(@"error: %@", [error localizedDescription]);
        }
    }];
    
    self.logInButton.center = self.view.center;
    [self.view addSubview:self.logInButton];
    
    NSString *die = [[NSBundle mainBundle] pathForResource:@"sfx_die" ofType: @"mp3"];
    self.dieURL = [[NSURL alloc] initFileURLWithPath:die];
    
    NSString *point = [[NSBundle mainBundle] pathForResource:@"sfx_point" ofType: @"mp3"];
    self.pointURL = [[NSURL alloc] initFileURLWithPath:point];
    
    self.dieAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:self.dieURL error:nil];
    //dieAudioPlayer.numberOfLoops = -1; //infinite loop
    
    self.pointAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:self.pointURL error:nil];
    
    self.clockView.text = 0;
    self.clockView.textColor = [UIColor redColor];
    self.clockView.font = [UIFont systemFontOfSize:20.0];
    
    if (self.userID) {
        NSLog(@"username: %@", self.username.text);
        self.logInButton.alpha = 0.0;
    } else {
        NSLog(@"not loggedin"); 
        self.username.alpha = 0.0;
    }
    
    //UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 44.0, 44.0)];
    //[button addTarget:self action:@selector(TwitterPost:) forControlEvents:UIControlEventAllTouchEvents];
    //[button setBackgroundColor:[UIColor blueColor]];
    //
    //[self.view addSubview:button];
    
    self.mi_magnitud=0;
    self.flag=0;
    
    // Data notifications are received through NSNotificationCenter.
    // Posted whenever a TLMMyo connects
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didConnectDevice:)
                                                 name:TLMHubDidConnectDeviceNotification
                                               object:nil];
    // Posted whenever a TLMMyo disconnects.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didDisconnectDevice:)
                                                 name:TLMHubDidDisconnectDeviceNotification
                                               object:nil];
    // Posted whenever the user does a successful Sync Gesture.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didSyncArm:)
                                                 name:TLMMyoDidReceiveArmSyncEventNotification
                                               object:nil];
    // Posted whenever Myo loses sync with an arm (when Myo is taken off, or moved enough on the user's arm).
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUnsyncArm:)
                                                 name:TLMMyoDidReceiveArmUnsyncEventNotification
                                               object:nil];
    // Posted whenever Myo is unlocked and the application uses TLMLockingPolicyStandard.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUnlockDevice:)
                                                 name:TLMMyoDidReceiveUnlockEventNotification
                                               object:nil];
    // Posted whenever Myo is locked and the application uses TLMLockingPolicyStandard.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didLockDevice:)
                                                 name:TLMMyoDidReceiveLockEventNotification
                                               object:nil];
    // Posted when a new orientation event is available from a TLMMyo. Notifications are posted at a rate of 50 Hz.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveOrientationEvent:)
                                                 name:TLMMyoDidReceiveOrientationEventNotification
                                               object:nil];
    // Posted when a new accelerometer event is available from a TLMMyo. Notifications are posted at a rate of 50 Hz.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveAccelerometerEvent:)
                                                 name:TLMMyoDidReceiveAccelerometerEventNotification
                                               object:nil];
    // Posted when a new pose is available from a TLMMyo.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceivePoseChange:)
                                                 name:TLMMyoDidReceivePoseChangedNotification
                                               object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSNotificationCenter Methods

-(void)updateValues{
    if (self.userID) {
        self.logInButton.alpha = 0.0;
        self.username.alpha = 1.0;
    } else {
        self.logInButton.alpha = 1.0;
        self.username.alpha = 0.0;
    }
}

- (void)didConnectDevice:(NSNotification *)notification {
    // Access the connected device.
    TLMMyo *myo = notification.userInfo[kTLMKeyMyo];
    NSLog(@"Connected to %@.", myo.name);

    // Align our label to be in the center of the view.
    [self.helloLabel setCenter:self.view.center];

    // Set the text of the armLabel to "Perform the Sync Gesture".
    self.armLabel.text = @"Perform the Sync Gesture";

    // Set the text of our helloLabel to be "Hello Myo".
    self.helloLabel.text = @"Hello Myo";

    // Show the acceleration progress bar
    [self.accelerationProgressBar setHidden:NO];
    [self.accelerationLabel setHidden:NO];
    [self.gauge setHidden:NO];
}

- (void)didDisconnectDevice:(NSNotification *)notification {
    // Access the disconnected device.
    TLMMyo *myo = notification.userInfo[kTLMKeyMyo];
    NSLog(@"Disconnected from %@.", myo.name);

    // Remove the text from our labels when the Myo has disconnected.
    self.helloLabel.text = @"";
    self.armLabel.text = @"";
    self.lockLabel.text = @"";
    [self.lockLabel setFrame:CGRectMake(0.0, 0.0, 200.0, 200.0)];

    // Hide the acceleration progress bar.
    [self.accelerationProgressBar setHidden:YES];
    [self.accelerationLabel setHidden:YES];
    [self.gauge setHidden: YES];
}

- (void)didUnlockDevice:(NSNotification *)notification {
    // Update the label to reflect Myo's lock state.
    self.lockLabel.text = @"Unlocked";
}

- (void)didLockDevice:(NSNotification *)notification {
    // Update the label to reflect Myo's lock state.
    
}

- (void)didSyncArm:(NSNotification *)notification {
    // Retrieve the arm event from the notification's userInfo with the kTLMKeyArmSyncEvent key.
    TLMArmSyncEvent *armEvent = notification.userInfo[kTLMKeyArmSyncEvent];

    // Update the armLabel with arm information.
    NSString *armString = armEvent.arm == TLMArmRight ? @"Right" : @"Left";
    NSString *directionString = armEvent.xDirection == TLMArmXDirectionTowardWrist ? @"Toward Wrist" : @"Toward Elbow";
    self.armLabel.text = [NSString stringWithFormat:@"Arm: %@ X-Direction: %@", armString, directionString];
    self.lockLabel.text = @"Locked";
}

- (void)didUnsyncArm:(NSNotification *)notification {
    // Reset the labels.
    self.armLabel.text = @"Perform the Sync Gesture";
    self.helloLabel.text = @"Hello Myo";
    self.lockLabel.text = @"";
    self.helloLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:50];
    self.helloLabel.textColor = [UIColor blackColor];
}

- (void)didReceiveOrientationEvent:(NSNotification *)notification {
    // Retrieve the orientation from the NSNotification's userInfo with the kTLMKeyOrientationEvent key.
    TLMOrientationEvent *orientationEvent = notification.userInfo[kTLMKeyOrientationEvent];

    //NSLog(@"orientation: %@", orientationEvent);
    
    // Create Euler angles from the quaternion of the orientation.
    TLMEulerAngles *angles = [TLMEulerAngles anglesWithQuaternion:orientationEvent.quaternion];

    // Next, we want to apply a rotation and perspective transformation based on the pitch, yaw, and roll.
    CATransform3D rotationAndPerspectiveTransform = CATransform3DConcat(CATransform3DConcat(CATransform3DRotate (CATransform3DIdentity, angles.pitch.radians, -1.0, 0.0, 0.0), CATransform3DRotate(CATransform3DIdentity, angles.yaw.radians, 0.0, 1.0, 0.0)), CATransform3DRotate(CATransform3DIdentity, angles.roll.radians, 0.0, 0.0, -1.0));

    // Apply the rotation and perspective transform to helloLabel.
    self.helloLabel.layer.transform = rotationAndPerspectiveTransform;
}

- (void)didReceiveAccelerometerEvent:(NSNotification *)notification {
    // Retrieve the accelerometer event from the NSNotification's userInfo with the kTLMKeyAccelerometerEvent.
    TLMAccelerometerEvent *accelerometerEvent = notification.userInfo[kTLMKeyAccelerometerEvent];

    //NSLog(@"accelerometerEvent: %@", accelerometerEvent);
    
    // Get the acceleration vector from the accelerometer event.
    TLMVector3 accelerationVector = accelerometerEvent.vector;

    // Calculate the magnitude of the acceleration vector.
    float magnitude = TLMVector3Length(accelerationVector);
    
    
    //Ralos Code
    if(self.flag==0){
        
        self.prevMagnitude = magnitude;
        self.flag=self.flag+1;
    }
    
    if (self.timeFlag%50==0 && self.timeFlag!=0) {
        self.clock=self.clock-1;

        //debug
        if (self.clock==19) {
            NSLog(@"started, is at 19 s");
        }
        
        if (self.clock==0) {
            NSLog(@"finished");
        }
    }
    
    if (magnitude>self.prevMagnitude && magnitude>2) {
        if (self.mi_magnitud<1) {
            self.mi_magnitud = self.mi_magnitud+magnitude/100;
        }
        
        self.timeFlag=self.timeFlag+1;
        if (self.doCountdown) {
            [self countdown];
            self.doCountdown = NO;
        }
    }
    
    else{
        
        if (self.mi_magnitud>0) {
            self.mi_magnitud = self.mi_magnitud-magnitude/100;
            
        }
    }

    if (self.isScore) {
        //NSLog(@"self:%f",self.mi_magnitud);
            //score controller
        if(self.mi_magnitud>0.46333&&self.mi_magnitud<0.60333){
            self.score=self.score+5;
            [self.pointAudioPlayer play];
            
        }
        else{
            //[self.dieAudioPlayer play];
            if (self.score>0) {
                self.score=self.score-1;
            }
        }
    }
    
    NSString* myNewString = [NSString stringWithFormat:@"%d", self.score];
    self.lockLabel.font = [UIFont systemFontOfSize:30.0];
    myNewString = [myNewString stringByAppendingString:@"   "];
    NSString *scoreLabel = [@"Score: " stringByAppendingString:myNewString];
    self.lockLabel.text = scoreLabel;
    

    //NSString* myClock = [NSString stringWithFormat:@"%d", self.clock];
    

    // Update the progress bar based on the magnitude of the acceleration vector.
    self.accelerationProgressBar.progress = self.mi_magnitud;

    /* Note you can also access the x, y, z values of the acceleration (in G's) like below
     float x = accelerationVector.x;
     float y = accelerationVector.y;
     float z = accelerationVector.z;
     */
}

-(void)countdown
{
    if (self.timer)
        return;

    self.remainingCounts = 15;
    [self updateLabel];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target: self selector: @selector(handleTimerTick) userInfo: nil repeats: YES];
}

-(void)handleTimerTick
{
    self.remainingCounts--;
    [self updateLabel];
    
    if (self.remainingCounts <= 0) {
        [self.timer invalidate];
        self.timer = nil;
    
        NSLog(@"TIME");
        
        self.isScore = NO;
        
        self.finish.alpha = 1.0;
        self.finish.text = @"TIME";
        self.finish.font = [UIFont systemFontOfSize:60.0];
        self.finish.textAlignment = NSTextAlignmentCenter;
        self.finish.textColor = [UIColor redColor];
        
        
        self.username.alpha = 0.0;
        self.clockView.alpha = 0.0;
        self.accelerationProgressBar.alpha = 0.0;
        self.gauge.alpha = 0.0;
        
        [self.tweetIt setHidden:NO];
        
        //
    }
}

-(void)updateLabel
{
    self.clockView.text = [[NSNumber numberWithUnsignedInt: self.remainingCounts] stringValue];
}

- (void)didReceivePoseChange:(NSNotification *)notification {
    // Retrieve the pose from the NSNotification's userInfo with the kTLMKeyPose key.
    TLMPose *pose = notification.userInfo[kTLMKeyPose];
    self.currentPose = pose;

    NSLog(@"pose: %@", pose);
    
    // Handle the cases of the TLMPoseType enumeration, and change the color of helloLabel based on the pose we receive.
    switch (pose.type) {
        case TLMPoseTypeUnknown:
        case TLMPoseTypeRest:
        case TLMPoseTypeDoubleTap:
            // Changes helloLabel's font to Helvetica Neue when the user is in a rest or unknown pose.
            self.helloLabel.text = @"Hello Myo";
            self.helloLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:50];
            self.helloLabel.textColor = [UIColor blackColor];
            break;
        case TLMPoseTypeFist:
            // Changes helloLabel's font to Noteworthy when the user is in a fist pose.
            self.helloLabel.text = @"Fist";
            self.helloLabel.font = [UIFont fontWithName:@"Noteworthy" size:50];
            self.helloLabel.textColor = [UIColor greenColor];
            break;
        case TLMPoseTypeWaveIn:
            // Changes helloLabel's font to Courier New when the user is in a wave in pose.
            self.helloLabel.text = @"Wave In";
            self.helloLabel.font = [UIFont fontWithName:@"Courier New" size:50];
            self.helloLabel.textColor = [UIColor greenColor];
            break;
        case TLMPoseTypeWaveOut:
            // Changes helloLabel's font to Snell Roundhand when the user is in a wave out pose.
            self.helloLabel.text = @"Wave Out";
            self.helloLabel.font = [UIFont fontWithName:@"Snell Roundhand" size:50];
            self.helloLabel.textColor = [UIColor greenColor];
            break;
        case TLMPoseTypeFingersSpread:
            // Changes helloLabel's font to Chalkduster when the user is in a fingers spread pose.
            self.helloLabel.text = @"Fingers Spread";
            self.helloLabel.font = [UIFont fontWithName:@"Chalkduster" size:50];
            self.helloLabel.textColor = [UIColor greenColor];
            break;
    }

    //// Unlock the Myo whenever we receive a pose
    //if (pose.type == TLMPoseTypeUnknown || pose.type == TLMPoseTypeRest) {
    //    // Causes the Myo to lock after a short period.
    //    [pose.myo unlockWithType:TLMUnlockTypeTimed];
    //} else {
    //    // Keeps the Myo unlocked until specified.
    //    // This is required to keep Myo unlocked while holding a pose, but if a pose is not being held, use
    //    // TLMUnlockTypeTimed to restart the timer.
    //    [pose.myo unlockWithType:TLMUnlockTypeHold];
    //    // Indicates that a user action has been performed.
    //    [pose.myo indicateUserAction];
    //}
}

-(void)TwitterLogOut:(id)sender{
}

-(IBAction)TwitterPost:(id)sender{
    self.client = [[TWTRAPIClient alloc] initWithUserID:self.userID];
    NSString *statusesShowEndpoint = @"https://api.twitter.com/1.1/statuses/update.json";
    NSDictionary *params = @{@"status" : [NSString stringWithFormat:@"A score of %d running with my #myo today!!", self.score]};
    NSError *clientError;

    NSURLRequest *request = [self.client URLRequestWithMethod:@"POST" URL:statusesShowEndpoint parameters:params error:&clientError];
    

    if (request) {
        [self.client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data) {
                // handle the response data e.g.
                NSError *jsonError;
                //NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                TwitterViewController *tweetController = [[TwitterViewController alloc] init];
                [self presentViewController:tweetController animated:YES completion:nil];
            }
            else {
                NSLog(@"Error: %@", connectionError);
            }
        }];
    }
    else {
        NSLog(@"Error: %@", clientError);
    }
}

- (IBAction)didTapSettings:(id)sender {
    // Note that when the settings view controller is presented to the user, it must be in a UINavigationController.
    UINavigationController *controller = [TLMSettingsViewController settingsInNavigationController];
    // Present the settings view controller modally.
    [self presentViewController:controller animated:YES completion:nil];
}

@end
